package co.com.sofka.crud.servicio.implementacion;

import co.com.sofka.crud.entidad.TareaEntity;
import co.com.sofka.crud.respositorio.TareaRepository;
import co.com.sofka.crud.convertidor.TareaConverter;
import co.com.sofka.crud.modelo.Tarea;
import co.com.sofka.crud.servicio.TareaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class TareaServiceImplementacion implements TareaService {

    @Autowired
    private TareaRepository tareaRepository;


    @Autowired
    private TareaConverter converter;


    @Override
    public Tarea findById(Long id) {
        return converter.entityToModel(tareaRepository.findById(id));
    }

    @Override
    public List<Tarea> findAll() {
        return converter.entityToModel((List<TareaEntity>) tareaRepository.findAll());
    }

    @Override
    public void deleteById(Long Id) {
    tareaRepository.deleteById(Id);
    }

    @Override
    public void save(Tarea tarea) {
        tareaRepository.save(converter.modelToEntity(tarea));
    }

    @Override
    public void deleteAll() {
            tareaRepository.deleteAll();

    }
}
