package co.com.sofka.crud.entidad;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity

@Table(name = "ListaTareas")
public class TareaEntity {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;

    @Column(name = "nombre")
    private String name;

    @Column(name = "completo")
    private boolean completed;

    @Column(name = "grouplistid")
    private String groupListId;

}
