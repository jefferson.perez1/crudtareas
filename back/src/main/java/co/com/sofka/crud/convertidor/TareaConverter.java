package co.com.sofka.crud.convertidor;

import co.com.sofka.crud.entidad.TareaEntity;
import co.com.sofka.crud.modelo.Tarea;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class TareaConverter {

    public Tarea entityToModel(TareaEntity tareaEntity) {

        Tarea tarea = new Tarea();

        tarea.setId(tareaEntity.getId());
        tarea.setName(tareaEntity.getName());
        tarea.setCompleted(tareaEntity.isCompleted());
        tarea.setGroupListId(tareaEntity.getGroupListId());

        return tarea;
    }

    public TareaEntity modelToEntity(Tarea tarea) {

        TareaEntity tareaEntity = new TareaEntity();

       tareaEntity.setId(tarea.getId());
       tareaEntity.setName(tarea.getName());
       tareaEntity.setCompleted(tarea.isCompleted());
       tareaEntity.setGroupListId(tarea.getGroupListId());

        return tareaEntity;
    }

    public List<Tarea> entityToModel(List<TareaEntity> tareaEntity){
        List<Tarea>tareas = new ArrayList<>(tareaEntity.size());
        tareaEntity.forEach(entity -> {
            tareas.add(entityToModel(entity));
        });
        return  tareas;
    }

    public Tarea entityToModel(Optional<TareaEntity> tareaEntity){
        Tarea tarea = new Tarea();
        if (tareaEntity.isPresent()){
            tarea.setId(tareaEntity.get().getId());
            tarea.setName(tareaEntity.get().getName());
            tarea.setCompleted(tareaEntity.get().isCompleted());
            tarea.setGroupListId(tareaEntity.get().getGroupListId());

        }
        return tarea;
    }

}
