package co.com.sofka.crud.servicio;

import co.com.sofka.crud.modelo.Tarea;

import java.util.List;

public interface TareaService {
    Tarea findById(Long id);
    List<Tarea> findAll();
    void deleteById(Long Id);
    void save(Tarea tarea);
    void deleteAll();

}
