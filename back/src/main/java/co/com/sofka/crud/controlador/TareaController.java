package co.com.sofka.crud.controlador;

import co.com.sofka.crud.servicio.TareaService;
import co.com.sofka.crud.modelo.Tarea;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
public class TareaController {


    @Autowired
    private TareaService tareaService;

    @GetMapping("/{id}/tarea")
    public Tarea findById(@PathVariable(value = "id", required = true) Long id){
        return tareaService.findById(id);
    }

    @GetMapping("/tareas")
    public List<Tarea> findAll(){
        return tareaService.findAll();
    }
    @DeleteMapping("{id}/tarea")
    public void deleteById(@PathVariable(value = "id", required = true) Long id){

        tareaService.deleteById(id);
    }

    @PostMapping("/tarea")
    public void insert(@RequestBody Tarea tarea){
        tareaService.save(tarea);
    }

    @PutMapping("/{id}/tarea")
    public void update(@RequestBody Tarea tarea, @PathVariable(value = "id",required = true) Long id){
        tarea.setId(id);
        tareaService.save(tarea);
    }

    @DeleteMapping("/tarea")
    public void deleteAll(){ tareaService.deleteAll();
    }
}
