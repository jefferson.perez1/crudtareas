package co.com.sofka.crud.respositorio;

import co.com.sofka.crud.entidad.TareaEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TareaRepository extends CrudRepository<TareaEntity, Long> {
}
