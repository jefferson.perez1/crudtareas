import React from 'react';
import { StoreProvider } from "./components/storeProvider/StoreProvider";
import  ListForm  from "./components/listForm/ListForm";
import  GroupTodo  from "./components/groupTodo/GroupTodo";


function App() {
  return <StoreProvider>
    <ListForm />
    <GroupTodo />
  </StoreProvider>
}

export default App;
