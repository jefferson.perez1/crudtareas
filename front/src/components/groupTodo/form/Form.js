import React, { useRef, useContext, useState } from "react";
import { Store } from "../../storeProvider/StoreProvider";

const HOST_API = "http://localhost:8080/api";

const Form = ({ groupId }) => {
  const formRef = useRef(null);
  const {
    dispatch,
    state: { todo }
  } = useContext(Store);
  const item = todo[groupId].item;
  const [state, setState] = useState(item);

  const onAdd = event => {
    
      event.preventDefault();
      if (state.name !== undefined && state.name.length > 0) {
      const request = {
        groupListId: groupId,
        name: state.name,
        id: null,
        isCompleted: false
      };
      fetch(HOST_API + "/todo", {
        method: "POST",
        body: JSON.stringify(request),
        headers: {
          "Content-Type": "application/json"
        }
      })
        .then(response => response.json())
        .then(todo => {
          dispatch({ type: "add-item", item: todo, groupId });
          setState({ name: "" });
          formRef.current.reset();
        });
    } else {
      alert("ingresa una tarea porfiss");
    }
  };

  const onEdit = event => {
    event.preventDefault();

    const request = {
      name: state.name,
      groupListId: groupId,
      id: item.id,
      isCompleted: item.isCompleted
    };

    fetch(HOST_API + "/todo", {
      method: "PUT",
      body: JSON.stringify(request),
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(response => response.json())
      .then(todo => {
        dispatch({ type: "update-item", item: todo, groupId });
        setState({ name: "" });
        formRef.current.reset();
      });
  };

  return (
    <form ref={formRef}>
      <input
        type="text"
        name="name"
        placeholder="¿Qué piensas hacer hoy?"
        defaultValue={item.name}
        onChange={event => {
          setState({ ...state, name: event.target.value });
        }}
      ></input>
      {item.id && <button onClick={onEdit}>Actualizar</button>}
      {!item.id && <button onClick={onAdd}>Crear</button>}
    </form>
  );
};

export default Form;
