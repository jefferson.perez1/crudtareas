import React, { useContext, } from "react";
import { Store } from "../storeProvider/StoreProvider";
import  Form  from "./form/Form";
import  List  from "./list/List";
const GroupTodo = () => {
    const { state } = useContext(Store);
    return <ul>
      {state.todo.map((list, index) => {
        return <ol key={index}>
          <fieldset>
            <legend>{list.name}</legend>
            <Form {...list} groupId={index} />
            <List {...list} groupId={index} />
          </fieldset>
        </ol>
      })}
    </ul>
  }

  export default GroupTodo;
