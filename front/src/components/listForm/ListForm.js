import React, { useContext, useRef, useState } from "react";
import { Store } from "../storeProvider/StoreProvider";

const ListForm = () => {
  const { dispatch } = useContext(Store);
  const formRef = useRef(null);
  const [state, setState] = useState({});

  const onCreate = event => {
    event.preventDefault();
    if (state.name !== undefined) {
      dispatch({ type: "new-list", name: state.name });
      formRef.current.reset();
      setState({});
    } else {
      alert("Debes ingresar un nombre a la lista");
    }
  };

  return (
    <form ref={formRef}>
      <input
        type="text"
        name="name"
        placeholder="Lista de TO-DO"
        onChange={event => {
          setState({ ...state, name: event.target.value });
        }}
      ></input>
      <button className="btn btn-danger" onClick={onCreate}>Nueva lista</button>
    </form>
  );
};

export default ListForm;
