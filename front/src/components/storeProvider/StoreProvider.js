import React, { useReducer, createContext} from "react";

const initialState = {
    todo: []
  };
const Store = createContext(initialState)

function reducer(state, action) {
  switch (action.type) {
    case 'new-list':
      const todo = state.todo;
      todo.push({ name: action.name, item: {}, list: [] });
      return { ...state, todo }
    case 'update-item':
      const todoUpItem = state.todo;
      const newListUpdate = todoUpItem[action.groupId];
      const listUpdateEdit = newListUpdate.list.map((item) => {
        if (item.id === action.item.id) {
          return action.item;
        }
        return item;
      });
      newListUpdate.list = listUpdateEdit;
      newListUpdate.item = {};
      return { ...state, todo: todoUpItem }
    case 'delete-item':
      const todoUpDelete = state.todo;
      const newListDelete = todoUpDelete[action.groupId];
      const listUpdate = newListDelete.list.filter((item) => {
        return item.id !== action.id;
      });
      newListDelete.list = listUpdate;
      return { ...state, todo: todoUpDelete }
    case 'update-list':
      const todoUpList = state.todo;
      const newListList = todoUpList[action.groupId];
      newListList.list = action.list;
      return { ...state, todo: todoUpList }
    case 'edit-item':
      const todoUpEdit = state.todo;
      const newListEdit = todoUpEdit[action.groupId];
      newListEdit.item = action.item;
      return { ...state, todo: todoUpEdit }
    case 'add-item':
      const todoUp = state.todo;
      const newList = todoUp[action.groupId].list;
      newList.push(action.item);
      return { ...state, todo: todoUp }
    default:
      return state;
  }
}

const StoreProvider = ({ children }) => {
    const [state, dispatch] = useReducer(reducer, initialState);
  
    return <Store.Provider value={{ state, dispatch }}>
      {children}
    </Store.Provider>
  
  }

  export {Store,StoreProvider}
